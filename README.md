# API App (client)

The API for the SFP app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode
```bash
npm run serve
```