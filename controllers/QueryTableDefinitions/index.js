/**
 * This file contains some table definitions to use with the node-sql module
 * It allows us to build complex queries with arrays and nested objects
 */
const sql = require('sql');
sql.setDialect('postgres');

exports.Employes = sql.define({
  name: 'employees',
  columns: [
    'firstname',
    'lastname',
    'birthdate',
    'place_of_birth',
    'nationality',
    'languages',
    'cnss_number',
    'identity_number',
    'filiale_id'
  ]
});