const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const PGPool = require('../database');

exports.register = async (req, res, next) => {
  try {
    // Check if user already exists
    const { firstName, lastName, mail, password, role, description } = req.body;
    const userCheck = await PGPool.query(
      'SELECT * FROM users WHERE mail = $1',
      [mail]
    );

    if (userCheck.rowCount > 0)
      return res.status(400).json({ message: 'This email is already in use' });

    const salt = await bcrypt.genSalt(10);
    const hasPassword = await bcrypt.hash(password, salt);
    const user = await PGPool.query(
      'INSERT INTO users(firstname, lastname, mail, password, role, description) VALUES($1, $2, $3, $4, $5, $6)',
      [firstName, lastName, mail, hasPassword, role, description]
    );

    res.status(201).json({ message: 'User successfully created!', user });
  } catch (error) {
    res.status(500).json({ message: 'Error when creating the user', error });
  }
};

exports.login = async (req, res, next) => {
  try {
    const { mail, password } = req.body;
    const userCheck = await PGPool.query(
      'SELECT * FROM users WHERE mail = $1',
      [mail]
    );
    if (userCheck.rowCount === 0)
      return res.status(404).json({ message: "This user doesn't exist" });

    const validPass = await bcrypt.compare(
      password,
      userCheck.rows[0].password
    );
    if (!validPass)
      return res
        .status(401)
        .json({ message: 'Email or Password is Incorrect' });

    // Create and assign token
    let payload = {
      id: userCheck.rows[0].id,
      mail: userCheck.rows[0].mail,
      firstName: userCheck.rows[0].firstname,
      lastName: userCheck.rows[0].lastname,
      description: userCheck.rows[0].description,
      role: userCheck.rows[0].userrole
    };
    const token = jwt.sign(payload, process.env.TOKEN_SECRET);
    res
      .status(200)
      .header('auth-token', token)
      .json({ token, userId: payload.id });
  } catch (error) {
    res.status(500).json({ message: 'Error when login', error });
  }
};

exports.getConnectedProfile = async (req, res, next) => {
  const { id } = req.params;
  try {
    const user = await PGPool.query('SELECT * FROM users WHERE id = $1', [id]);
    res.status(200).json(user.rows[0]);
  } catch(error) {
    res.status(500).json({ message: "Error fetching user profile"});
  }


}

exports.changeUserPassword = async (req, res, next) => {
  const { oldpassword, newpassword, newpasswordconfirm } = req.body;
  const { id } = req.user;

  const user = await PGPool.query('SELECT * FROM users WHERE id = $1',[id]);

  // Check if the old password provided is correct
  const validPass = await bcrypt.compare( oldpassword, user.rows[0].password);
  if (!validPass) return res.status(401).json({ message: 'Password is Incorrect' });

  // Check if newpassword and newpasswordconfirm are equals
  if(newpassword !== newpasswordconfirm) return res.status(401).json({ message: 'Passwords are differents' });

  // Set the new password for the user
  const salt = await bcrypt.genSalt(10);
  const hasPassword = await bcrypt.hash(newpasswordconfirm, salt);

  try {
    await PGPool.query('UPDATE users SET password = $1 WHERE id = $2', [hasPassword, id]);
    return res.status(200).json({ message: 'Successfully changed password!'});
  } catch(error) {
    return res.status(500).json({ message: 'Error while changing password!', error });
  }
}

exports.updateUser = async (req, res, next) => {
  const { id } = req.user;
  //const { firstname, lastname, description, mail, lastlogin } = req.body;
  const firstname = req.body.firstname || req.user.firstName;
  const lastname = req.body.lastname || req.user.lastName;
  const description = req.body.description || req.user.description;
  const mail = req.body.mail || req.user.mail;
  const lastlogin = req.body.lastlogin || null;

  try {
    await PGPool.query('UPDATE users SET firstname = $1, lastname = $2, description = $3, mail = $4, lastlogin = $5 WHERE id = $6',
    [firstname, lastname, description, mail, lastlogin, id]);
    return res.status(200).json({ message: 'Successfully updated user!'});
  } catch(error) {
    return res.status(500).json({ message: 'Error while updating user!', error });
  }
}
