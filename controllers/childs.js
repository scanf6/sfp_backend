const PGPool = require('../database');

exports.createChild = async (req, res, next) => {
  const {
    firstname,
    lastname,
    employe_id
  } = req.body;

  try {
    await PGPool.query('INSERT INTO childs (firstname, lastname, employee_id) VALUES ($1, $2, $3)',
    [ firstname, lastname, employe_id ]);
    return res.status(201).json({ message: 'Child created successfully !'})
  } catch (error) {
    return res.status(500).json({ message: 'Error when saving the child!', error });
  }
};

exports.getChilds = async (req, res, next) => {
  const { filter:name, page, rowsPerPage } = req.query;
  try {
    let count = await PGPool.query('SELECT COUNT(id) FROM childs'); // For statistics
    let childs = await PGPool.query(`SELECT childs.*, employees.firstname as employefirstname, employees.lastname as employelastname FROM childs INNER JOIN employees ON childs.employee_id = employees.id WHERE (LOWER(childs.firstname) LIKE LOWER($1) OR LOWER(childs.lastname) LIKE LOWER($1)) LIMIT $2 OFFSET $3`,
    [`%${name}%`, rowsPerPage == 0 ? null : rowsPerPage, rowsPerPage == 0 ? null : ((page - 1) * rowsPerPage)]); // For actual table rows

    let filteredChilds = await PGPool.query(`SELECT * FROM childs WHERE (LOWER(childs.firstname) LIKE LOWER($1) OR LOWER(childs.lastname) LIKE LOWER($1))`,
    [`%${name}%`]); // For table pagination

    return res.status(200).json({ results: childs.rows, count: filteredChilds.rowCount, allCount: count.rows[0]['count'] });
  } catch (error) {
    return res
      .status(500)
      .json({ message: 'Error while fetching the childs', error });
  }
};

exports.deleteChild = async (req, res, next) => {
  const { id } = req.params;
  try {
    await PGPool.query('DELETE FROM childs WHERE id = $1', [id]);
    return res.status(200).json({ message: 'Successfuly deleted child' });
  } catch (error) {
    return res.status(500).json({ message: 'Error while deleting the child', error });
  }
};

exports.putChild = async (req, res, next) => {
  const { id } = req.params;
  const {
    firstname,
    lastname,
    employe_id
  } = req.body;

  try {
    await PGPool.query('UPDATE childs SET firstname = $1, lastname = $2, employee_id = $3 WHERE id = $4',
    [ firstname, lastname, employe_id, id]);
    return res.status(200).json({ message: 'Successfuly updated child' });
  } catch (error) {
    return res.status(500).json({ message: 'Error while updating the child', error });
  }
};
