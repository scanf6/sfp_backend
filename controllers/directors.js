const PGPool = require('../database');
const bcrypt = require('bcryptjs')

exports.createDirector = async (req, res, next) => {
  const { firstname, lastname, mail, password, description } = req.body;

  try {
    const salt = await bcrypt.genSalt(10);
    const hasPassword = await bcrypt.hash(password, salt);
    await PGPool.query('INSERT INTO users(firstname, lastname, description, mail, password, role) VALUES($1, $2, $3, $4, $5, $6) RETURNING id',
    [firstname, lastname, description, mail, hasPassword, 'director']);
    return res.status(201).json({ message: 'Director Successfully created!'});
  } catch (error) {
    return res.status(500).json({ message: 'Error when creating the director!', error });
  }
};

exports.getDirectors = async (req, res, next) => {
  const { filter:name, page, rowsPerPage } = req.query;
  try {
    let count = await PGPool.query('SELECT COUNT(id) FROM users WHERE role = $1', ['director']); // For statistics
    let directors = await PGPool.query('SELECT * FROM users WHERE role = $1 AND (LOWER(firstname) LIKE LOWER($2) OR LOWER(lastname) LIKE LOWER($2)) LIMIT $3 OFFSET $4',
    ['director', `%${name}%`, rowsPerPage == 0 ? null : rowsPerPage, rowsPerPage == 0 ? null : ((page - 1) * rowsPerPage)]); // For actual table rows

    let filteredDirectors = await PGPool.query('SELECT * FROM users WHERE role = $1 AND (LOWER(firstname) LIKE LOWER($2) OR LOWER(lastname) LIKE LOWER($2))', ['director', `%${name}%`]); // For table pagination

    return res.status(200).json({ results: directors.rows, count: filteredDirectors.rowCount, allCount: count.rows[0]['count'] });
  } catch (error) {
    return res
      .status(500)
      .json({ message: 'Error while fetching the filiales', error });
  }
};

exports.putDirector = async (req, res, next) => {
  const { id } = req.params;
  const { firstname, lastname, description, mail, password } = req.body;
  const salt = await bcrypt.genSalt(10);
  const hasPassword = await bcrypt.hash(password, salt);

  try {
    await PGPool.query('UPDATE users SET firstname = $1, lastname = $2, description = $3, mail = $4, password = $5 WHERE id = $6', [firstname, lastname, description, mail, hasPassword, id]);
    return res.status(200).json({ message: 'Successfuly updated director' });
  } catch (error) {
    return res.status(500).json({ message: 'Error while updating the director', error });
  }
};

exports.deleteDirector = async (req, res, next) => {
  const { id } = req.params;
  try {
    await PGPool.query('DELETE FROM users WHERE id = $1', [id]);
    return res.status(200).json({ message: 'Successfuly deleted director' });
  } catch (error) {
    return res.status(500).json({ message: 'Error while deleting the director', error });
  }
};
