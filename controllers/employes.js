const PGPool = require('../database');
const { Employes } = require('./QueryTableDefinitions');

exports.createEmploye = async (req, res, next) => {
  const {
    firstname,
    lastname,
    birthdate,
    place_of_birth,
    nationality,
    languages,
    cnss_number,
    identity_number,
    filiale_id
  } = req.body;

  try {
    //let query = Employes.insert([employe]).toQuery();
    //console.log(query);
    await PGPool.query('INSERT INTO employees (firstname, lastname, birthdate, place_of_birth, nationality, languages, cnss_number, identity_number, filiale_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)',
    [ firstname, lastname, birthdate, place_of_birth, nationality, languages, cnss_number, identity_number, filiale_id ]);
    return res.status(201).json({ message: 'Employee created successfully !'})
  } catch (error) {
    return res.status(500).json({ message: 'Error when saving the employe!', error });
  }
};

exports.getEmployes = async (req, res, next) => {
  const { filter:name, filterByFiliale, page, rowsPerPage } = req.query;
  try {
    let count = await PGPool.query('SELECT COUNT(id) FROM employees'); // For statistics

    let employes = await PGPool.query(`SELECT employees.*, filiales.name as filialename FROM employees INNER JOIN filiales ON employees.filiale_id = filiales.id WHERE (LOWER(firstname) LIKE LOWER($1) OR LOWER(lastname) LIKE LOWER($1)) AND ${filterByFiliale == 0 ? 'TRUE' : 'employees.filiale_id'} = $2 LIMIT $3 OFFSET $4`,
    [`%${name}%`, filterByFiliale == 0 ? 'TRUE' : filterByFiliale, rowsPerPage == 0 ? null : rowsPerPage, rowsPerPage == 0 ? null : ((page - 1) * rowsPerPage)]); // For actual table rows


    let filteredEmployes = await PGPool.query(`SELECT * FROM employees WHERE (LOWER(firstname) LIKE LOWER($1) OR LOWER(lastname) LIKE LOWER($1)) AND ${filterByFiliale == 0 ? 'TRUE' : 'filiale_id'} = $2`,
    [`%${name}%`, filterByFiliale == 0 ? 'TRUE' : filterByFiliale]); // For table pagination

    return res.status(200).json({ results: employes.rows, count: filteredEmployes.rowCount, allCount: count.rows[0]['count'] });
  } catch (error) {
    return res
      .status(500)
      .json({ message: 'Error while fetching the employees', error });
  }
};

exports.deleteEmploye = async (req, res, next) => {
  const { id } = req.params;
  try {
    await PGPool.query('DELETE FROM employees WHERE id = $1', [id]);
    return res.status(200).json({ message: 'Successfuly deleted employee' });
  } catch (error) {
    return res.status(500).json({ message: 'Error while deleting the employee', error });
  }
};

exports.putEmploye = async (req, res, next) => {
  const { id } = req.params;
  const {
    firstname,
    lastname,
    birthdate,
    place_of_birth,
    nationality,
    languages,
    cnss_number,
    identity_number,
    filiale_id
  } = req.body;

  try {
    await PGPool.query('UPDATE employees SET firstname = $1, lastname = $2, birthdate = $3, place_of_birth = $4, nationality = $5, languages = $6, cnss_number = $7, identity_number = $8, filiale_id = $9 WHERE id = $10',
    [ firstname, lastname, birthdate, place_of_birth, nationality, languages, cnss_number, identity_number, filiale_id, id]);
    return res.status(200).json({ message: 'Successfuly updated employee' });
  } catch (error) {
    return res.status(500).json({ message: 'Error while updating the employee', error });
  }
};

exports.getEmployesFromFiliale = async (req, res, next) => {
  const {id} = req.params;

  try {
    const employes = await PGPool.query('SELECT employees.*, filiales.name as filialename FROM employees INNER JOIN filiales ON employees.filiale_id = filiales.id WHERE employees.filiale_id = $1',[id]);
    return res.status(200).json(employes.rows);
  } catch (error) {
    return res.status(500).json({ message: 'Error while updating the employee', error });
  }
}
