const PGPool = require('../database');

exports.createFiliale = async (req, res, next) => {
  const { name } = req.body;

  try {
    await PGPool.query('INSERT INTO filiales(name) VALUES($1) RETURNING id', [name]);
    return res.status(201).json({ message: 'Filiale Successfully created!'});
  } catch (error) {
    return res.status(500).json({ message: 'Error when creating the filiale!', error });
  }
};

exports.getFiliales = async (req, res, next) => {
  const { filter:name, page, rowsPerPage } = req.query;
  try {
    let count = await PGPool.query('SELECT COUNT(id) FROM filiales'); // For statistics
    let filiales = await PGPool.query('SELECT filiales.*, COUNT(employees) AS numberemployees FROM filiales LEFT JOIN employees ON filiales.id = employees.filiale_id WHERE LOWER(name) LIKE LOWER($1) GROUP BY filiales.id LIMIT $2 OFFSET $3',
    [`%${name}%`, rowsPerPage == 0 ? null : rowsPerPage, rowsPerPage == 0 ? null : ((page - 1) * rowsPerPage)]); // For actual table rows

    let filteredFiliales = await PGPool.query('SELECT * FROM filiales WHERE LOWER(name) LIKE LOWER($1)', [`%${name}%`]); // For table pagination

    return res.status(200).json({ results: filiales.rows, count: filteredFiliales.rowCount, allCount: count.rows[0]['count'] });
  } catch (error) {
    return res
      .status(500)
      .json({ message: 'Error while fetching the filiales', error });
  }
};

exports.putFiliale = async (req, res, next) => {
  const { id } = req.params;
  const { name } = req.body;

  try {
    await PGPool.query('UPDATE filiales SET name = $1 WHERE id = $2', [name, id]);
    return res.status(200).json({ message: 'Successfuly updated filiale' });
  } catch (error) {
    return res.status(500).json({ message: 'Error while updating the filiale', error });
  }
};

exports.deleteFiliale = async (req, res, next) => {
  const { id } = req.params;
  try {
    await PGPool.query('DELETE FROM filiales WHERE id = $1', [id]);
    return res.status(200).json({ message: 'Successfuly deleted filiale' });
  } catch (error) {
    return res.status(500).json({ message: 'Error while deleting the employee', error });
  }
};
