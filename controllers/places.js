const PGPool = require('../database');

exports.getPlaces = async (req, res, next) => {
  const places = await PGPool.query('SELECT * FROM places');
  return res.status(200).json(places.rows);
};

exports.createPlace = async (req, res, next) => {
  const {
    name,
    description,
    image,
    lat,
    long,
  } = req.body;


};
