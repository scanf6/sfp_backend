const PGPool = require('../database');

exports.getTodos = async (req, res, next) => {
  const todos = await PGPool.query('SELECT * FROM todos');
  return res.status(200).json(todos.rows);
};

exports.createTodo = async (req, res, next) => {
  const {
    title,
    description,
    completed,
  } = req.body;

  await PGPool.query('INSERT INTO todos (title, description, completed) VALUES ($1, $2, $3)',
    [ title, description, completed ]);

    return res.status(201).json({ message: 'Todo created successfully !'})
};

exports.deleteTodo = async (req, res, next) => {
  const { id } = req.params;

  try {
    await PGPool.query('DELETE FROM todos WHERE id = $1', [id]);
    return res.status(200).json({ message: 'Successfuly deleted todo' });
  } catch (error) {
    return res.status(500).json({ message: 'Error while deleting the todo', error });
  }
};
