const Pool = require('pg').Pool;
const env = process.env.NODE_ENV;

const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
  ssl: (!env || env === 'development') ?
    null :
    { rejectUnauthorized: false }
});

module.exports = pool;
