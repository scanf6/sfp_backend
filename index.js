// Modules & Packages
const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const app = express();
const cors = require('cors');
dotenv.config();

// Routes
const authRouter = require('./routes/auth');
const filialesRouter = require('./routes/filiales');
const employesRouter = require('./routes/employes');
const childsRouter = require('./routes/childs');
const directorsRouter = require('./routes/directors');
const placesRouter = require('./routes/places');
const todosRouter = require('./routes/todos');

// Middlewares
const { verifyToken } = require('./middlewares/tokens');

app.use(cors());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.json());
app.use('/', express.static('public'));
app.use('/api/auth', authRouter);
app.use('/api/filiales', filialesRouter);
app.use('/api/employes', employesRouter);
app.use('/api/childs', verifyToken, childsRouter);
app.use('/api/directors', verifyToken, directorsRouter);
app.use('/api/places', placesRouter);
app.use('/api/todos', todosRouter);
app.get('/apitest', (req, res, next) => {
  return res.status(200).json({ message: "This is returned by an API" });
});

// Sending the Client code for every GET request to use the Quasar Routing system
app.get('*', function(req, res){
  res.sendFile('index.html', {root: path.join(__dirname, 'public/')});
});

// Server Init
const port = process.env.PORT || 5000;
app.listen(port, function () {
  console.log(`Server listening on port ${port}`);
});
