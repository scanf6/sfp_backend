const Joi = require('joi');

exports.validateRegisterUser = (req, res, next) => {
  const data = req.body;
  const schema = Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    mail: Joi.string().email().required(),
    password: Joi.string().required(),
    role: Joi.string().valid('admin', 'director').required(),
    description: Joi.string().required(),
  });

  const validatedSchema = schema.validate(data);
  if (validatedSchema.error || validatedSchema.errors) {
    return res.status(400).json({
      message: '(Back-end) Provided fields are incrrect',
      detail: validatedSchema.error.details[0].message
    });
  }

  return next();
};

exports.validateLoginUser = (req, res, next) => {
  const data = req.body;
  const schema = Joi.object({
    mail: Joi.string().email().required(),
    password: Joi.string().required()
  });

  const validatedSchema = schema.validate(data);
  if (validatedSchema.error || validatedSchema.errors) {
    return res.status(400).json({
      message: '(Back-end) Provided fields are incorrect',
      detail: validatedSchema.error.details[0].message
    });
  }

  return next();
};

exports.validateCreateFiliale = (req, res, next) => {
  const data = req.body;
  const schema = Joi.object({
    name: Joi.string().required(),
  });

  // schema options
  const options = {
    abortEarly: false,
    allowUnknown: true,
    stripUnknown: true
  };

  const validatedSchema = schema.validate(data, options);
  if (validatedSchema.error || validatedSchema.errors) {
    return res.status(400).json({
      message: '(Back-end) Provided fields are incorrect',
      detail: validatedSchema.error.details[0].message
    });
  }

  return next();
};

exports.validateCreateDirector = (req, res, next) => {
  const data = req.body;
  const schema = Joi.object({
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    description: Joi.string().required(),
    mail: Joi.string().email().required(),
    password: Joi.string().required(),
    role: Joi.string().required(),
  });

  // schema options
  const options = {
    abortEarly: false,
    allowUnknown: true,
    stripUnknown: true
  };

  const validatedSchema = schema.validate(data, options);
  if (validatedSchema.error || validatedSchema.errors) {
    return res.status(400).json({
      message: '(Back-end) Provided fields are incorrect',
      detail: validatedSchema.error.details[0].message
    });
  }

  return next();
};

exports.validateCreateEmploye = (req, res, next) => {
  const data = req.body;
  const schema = Joi.object({
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    birthdate: Joi.string(),
    place_of_birth: Joi.string().required(),
    nationality: Joi.string().required(),
    cnss_number: Joi.string().required(),
    identity_number: Joi.string().required(),
    filiale_id: Joi.number().required(),
    languages: Joi.required(),
  });

  // schema options
  const options = {
    abortEarly: false,
    allowUnknown: true,
    stripUnknown: true
  };

  const validatedSchema = schema.validate(data, options);
  if (validatedSchema.error || validatedSchema.errors) {
    return res.status(400).json({
      message: '(Back-end) Provided fields are incorrect',
      detail: validatedSchema.error.details[0].message
    });
  }

  return next();
};

exports.validateCreateChild = (req, res, next) => {
  const data = req.body;
  const schema = Joi.object({
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    employe_id: Joi.number().required(),
  });

  // schema options
  const options = {
    abortEarly: false,
    allowUnknown: true,
    stripUnknown: true
  };

  const validatedSchema = schema.validate(data, options);
  if (validatedSchema.error || validatedSchema.errors) {
    return res.status(400).json({
      message: '(Back-end) Provided fields are incorrect',
      detail: validatedSchema.error.details[0].message
    });
  }

  return next();
};

exports.validateChangePassword = (req, res, next) => {
  const data = req.body;
  const schema = Joi.object({
    oldpassword: Joi.string().required(),
    newpassword: Joi.string().required(),
    newpasswordconfirm: Joi.string().required(),
  });

  // schema options
  const options = {
    abortEarly: false,
    allowUnknown: true,
    stripUnknown: true
  };

  const validatedSchema = schema.validate(data, options);
  if (validatedSchema.error || validatedSchema.errors) {
    return res.status(400).json({
      message: '(Back-end) Provided fields are incorrect',
      detail: validatedSchema.error.details[0].message
    });
  }

  return next();
};
