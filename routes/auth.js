const router = require('express').Router();
const { register, login, getConnectedProfile, changeUserPassword, updateUser } = require('../controllers/auth');
const { validateRegisterUser, validateLoginUser, validateChangePassword } = require('../middlewares/validation');
const { verifyToken } = require('../middlewares/tokens');

router.post('/register', validateRegisterUser, register);
router.post('/login', validateLoginUser, login);
router.get('/profile/:id', verifyToken, getConnectedProfile);
router.post('/changepassword', verifyToken, validateChangePassword, changeUserPassword);
router.patch('/updateuser', verifyToken, updateUser);

module.exports = router;
