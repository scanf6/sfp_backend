const router = require('express').Router();
const { createChild, getChilds, deleteChild, putChild } = require('../controllers/childs');
const { verifyToken } = require('../middlewares/tokens');
const { validateCreateChild } = require('../middlewares/validation');

router.route('/').get(verifyToken, getChilds).post(verifyToken, validateCreateChild, createChild);
router.route('/:id').delete(verifyToken, deleteChild).put(verifyToken, validateCreateChild, putChild);

module.exports = router;
