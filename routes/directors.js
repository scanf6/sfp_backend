const router = require('express').Router();
const {
  createDirector,
  getDirectors,
  putDirector,
  deleteDirector
} = require('../controllers/directors');

const { validateCreateDirector } = require('../middlewares/validation');
const { verifyToken } = require('../middlewares/tokens');

router.route('/').post(verifyToken, validateCreateDirector, createDirector).get(verifyToken, getDirectors);

router.route('/:id').delete(verifyToken, deleteDirector).put(verifyToken, validateCreateDirector, putDirector);

module.exports = router;
