const router = require('express').Router();
const { createEmploye, getEmployes, deleteEmploye, putEmploye, getEmployesFromFiliale } = require('../controllers/employes');
const { verifyToken } = require('../middlewares/tokens');
const { validateCreateEmploye } = require('../middlewares/validation');

router.route('/').get(verifyToken, getEmployes).post(verifyToken, validateCreateEmploye, createEmploye);
router.route('/:id').delete(verifyToken, deleteEmploye).put(verifyToken, validateCreateEmploye, putEmploye);
router.route('/filiale/:id').get(verifyToken, getEmployesFromFiliale);

module.exports = router;
