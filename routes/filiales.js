const router = require('express').Router();
const {
  createFiliale,
  getFiliales,
  putFiliale,
  deleteFiliale
} = require('../controllers/filiales');

const { validateCreateFiliale } = require('../middlewares/validation');
const { verifyToken } = require('../middlewares/tokens');

router.route('/').post(verifyToken, validateCreateFiliale, createFiliale).get(verifyToken, getFiliales);

router.route('/:id').delete(verifyToken, deleteFiliale).put(verifyToken, validateCreateFiliale, putFiliale);

module.exports = router;
