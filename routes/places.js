const router = require('express').Router();
const { getPlaces, createPlace } = require('../controllers/places');

router.route('/').get(getPlaces).post(createPlace);

module.exports = router;
