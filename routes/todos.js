const router = require('express').Router();
const { getTodos, createTodo, deleteTodo } = require('../controllers/todos');

router.route('/').get(getTodos).post(createTodo);
router.route('/:id').delete(deleteTodo);

module.exports = router;
